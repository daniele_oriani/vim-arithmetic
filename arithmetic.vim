" Number of significant digits used in output (set to 0 to use integers)
let g:arithmetic_precision = 9

" Perform algebric sum of addend and number under cursor position
" and replace it with the result
function! AlgSum(addend)
  let l:n_bk = @n
  let l:re_bk = @/
  let l:re = '\(-\|+\)\=\<\d\+\.\=\d*e\=\(-\|+\)\=\d*\>'
  let @/ = l:re
  execute('normal! gN"ny')
  let l:res = (str2float(@n)) + (str2float(a:addend))
  let l:islastword = match(getline("."), @n . '$')
  let @n = printf("%." . g:arithmetic_precision . "g", l:res)
  if l:islastword != -1
    let @n = " " . @n
  endif
  execute('normal! gvx"nP')
  let @n = l:n_bk
  let @/ = l:re_bk
endfunction

" Perform multiplication of factor and number under cursor position
" and replace it with the result
function! MultiplyBy(factor)
  let l:n_bk = @n
  let l:re_bk = @/
  let l:re = '\(-\|+\)\=\<\d\+\.\=\d*e\=\(-\|+\)\=\d*\>'
  let @/ = l:re
  execute('normal! gN"ny')
  let l:res = (str2float(@n)) * (str2float(a:factor))
  let l:islastword = match(getline("."), @n . '$')
  let @n = printf("%." . g:arithmetic_precision . "g", l:res)
  if l:islastword != -1
    let @n = " " . @n
  endif
  execute('normal! gvx"nP')
  let @n = l:n_bk
  let @/ = l:re_bk
endfunction

" Perform divide number under cursor position by div
" and replace it with the result
function! DivideBy(div)
  let l:n_bk = @n
  let l:re_bk = @/
  let l:re = '\(-\|+\)\=\<\d\+\.\=\d*e\=\(-\|+\)\=\d*\>'
  let @/ = l:re
  execute('normal! gN"ny')
  let l:res = (str2float(@n)) / (str2float(a:div))
  let l:islastword = match(getline("."), @n . '$')
  let @n = printf("%." . g:arithmetic_precision . "g", l:res)
  if l:islastword != -1
    let @n = " " . @n
  endif
  execute('normal! gvx"nP')
  let @n = l:n_bk
  let @/ = l:re_bk
endfunction
